
var exContent = (function (my) {

    my.init = function () {

        showSpinner();


        setTimeout(function () {
            document.getElementById("iFrameDiv").remove();
            document.getElementById("modalDiv").remove();
        }, 1000);
    }

    function showSpinner() {

        wrapperDiv = document.createElement("div");
        wrapperDiv.id = "iFrameDiv";
        wrapperDiv.setAttribute("style", "position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255); opacity: 0.5; z-index: 2000; height: 1083px; width: 100%;");

        iframeElement = document.createElement("iframe");
        iframeElement.setAttribute("style", "width: 100%; height: 100%;");

        wrapperDiv.appendChild(iframeElement);

        modalDialogParentDiv = document.createElement("div");
        modalDialogParentDiv.id = "modalDiv";
        modalDialogParentDiv.setAttribute("style", "position: absolute;border-radius:5px; width: 120px; border: 1px solid #919496; padding: 10px; background-color: rgb(255, 255, 255); z-index: 2001; overflow: auto; text-align: center; top: 149px; left: 45%;");

        modalDialogSiblingDiv = document.createElement("div");

        modalDialogTextDiv = document.createElement("div");
        modalDialogTextDiv.setAttribute("style", "text-align:center");

        modalDialogTextSpan = document.createElement("span");
        modalDialogText = document.createElement("strong");
        modalDialogText.innerHTML = "Please Wait...";

        breakElement = document.createElement("br");
        imageElement = document.createElement("img");
        imageElement.setAttribute("style", "width:32px;height:32px;");
        imageElement.src = chrome.extension.getURL("img/spinner.gif");
       

        modalDialogTextSpan.appendChild(modalDialogText);
        modalDialogTextDiv.appendChild(modalDialogTextSpan);
        modalDialogTextDiv.appendChild(breakElement);
        modalDialogTextDiv.appendChild(breakElement);
        modalDialogTextDiv.appendChild(imageElement);

        modalDialogSiblingDiv.appendChild(modalDialogTextDiv);
        modalDialogParentDiv.appendChild(modalDialogSiblingDiv);

        document.body.appendChild(wrapperDiv);
        document.body.appendChild(modalDialogParentDiv);
    }

    return my;
})(exContent || {});

exContent.init()

